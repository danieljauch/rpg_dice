defmodule RPGDice.DiceTest do
  use ExUnit.Case
  use Rop
  alias RPGDice.Dice

  describe "new/3" do
    test "responds with error when dice is given invalid sides" do
      assert {:error, _} = Dice.new(-1)
      assert {:error, _} = Dice.new(0)
      assert {:error, _} = Dice.new(1)
    end

    test "can generically make a new dice with any number of sides" do
      assert {:ok, %Dice{range: 1..3}} = Dice.new(3)
      assert {:ok, %Dice{range: 1..17}} = Dice.new(17)
      assert {:ok, %Dice{range: 1..40}} = Dice.new(40)
    end

    test "properly makes a coin, as long as the dice sides aren't invalid" do
      assert {:error, _} = Dice.new(1, "coin")

      assert {:ok,
              %Dice{range: 0..1, name: "coin", side_names: ~w(tails heads)}} =
               Dice.new(2, "coin")

      assert {:ok,
              %Dice{range: 0..1, name: "coin", side_names: ~w(tails heads)}} =
               Dice.new(3, "coin")
    end

    test "can make a fate or fudge dice" do
      assert {:ok,
              %Dice{
                range: [-1, -1, 0, 0, 1, 1],
                name: "fate",
                side_names: ["-", "-", " ", " ", "+", "+"]
              }} = Dice.new(6, "fate")

      assert {:ok,
              %Dice{
                range: [-1, -1, 0, 0, 1, 1],
                name: "fudge",
                side_names: ["-", "-", " ", " ", "+", "+"]
              }} = Dice.new(6, "fudge")
    end

    test "takes a name for custom dice" do
      assert {:ok, %Dice{range: 1..3, name: "triple-die"}} =
               Dice.new(3, "triple-die")
    end

    test "takes side names as long as the length matches the sides" do
      assert {:ok,
              %Dice{
                range: 1..3,
                name: "triple-die",
                side_names: ~w(left middle right)
              }} = Dice.new(3, "triple-die", ~w(left middle right))

      assert {:error, _} = Dice.new(3, "triple-die", ~w(first second))
    end
  end

  describe "by_name/1" do
    test "only uses valid names" do
      assert {:error, _} = Dice.by_name("foo")
    end

    test "properly builds a coin" do
      assert {:ok,
              %Dice{range: 0..1, name: "coin", side_names: ~w(tails heads)}} =
               Dice.by_name("coin")
    end

    test "properly builds polyhedral dice" do
      assert {:ok, %Dice{range: 1..2, name: "d2"}} = Dice.by_name("d2")
      assert {:ok, %Dice{range: 1..4, name: "d4"}} = Dice.by_name("d4")
      assert {:ok, %Dice{range: 1..6, name: "d6"}} = Dice.by_name("d6")
      assert {:ok, %Dice{range: 1..8, name: "d8"}} = Dice.by_name("d8")
      assert {:ok, %Dice{range: 1..10, name: "d10"}} = Dice.by_name("d10")
      assert {:ok, %Dice{range: 1..12, name: "d12"}} = Dice.by_name("d12")
      assert {:ok, %Dice{range: 1..20, name: "d20"}} = Dice.by_name("d20")
      assert {:ok, %Dice{range: 1..100, name: "d100"}} = Dice.by_name("d100")
      assert {:ok, %Dice{range: 1..100, name: "d%"}} = Dice.by_name("d%")
    end
  end
end
