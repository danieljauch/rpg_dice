defmodule RPGDice.RollTest do
  use ExUnit.Case
  use Rop
  alias RPGDice.{Dice, Roll}

  setup do
    max_result = 10
    {:ok, dice} = Dice.new(max_result)
    max_named_result = 3

    {:ok, named_dice} =
      Dice.new(max_named_result, "triple-die", ~w(left middle right))

    pool_count = 3
    pool = Roll.pool([{pool_count, dice}])
    named_pool = Roll.pool([{pool_count, named_dice}])

    {:ok,
     max_result: max_result,
     max_named_result: max_named_result,
     dice: dice,
     named_dice: named_dice,
     pool: pool,
     named_pool: named_pool}
  end

  describe "result/1" do
    test "can get a result in the range", %{dice: %Dice{range: range} = dice} do
      assert Roll.result(dice) in range
    end

    test "can get different results rolling multiple times", %{dice: dice} do
      first_result = Roll.result(dice)

      refute Enum.all?(1..10, fn _ ->
               Roll.result(dice) == first_result
             end)
    end

    test "can get a named result when the sides have names", %{
      named_dice: %Dice{side_names: side_names} = named_dice
    } do
      assert Roll.result(named_dice) in side_names
    end
  end

  describe "pool/2" do
    test "can take a count with a single dice type and get that count back", %{
      dice: dice
    } do
      count = 3
      pool = Roll.pool([{count, dice}])

      assert length(pool) == count

      assert Enum.all?(pool, fn current_dice ->
               current_dice == dice
             end)
    end

    test "can take multiple counts with multiple dice", %{
      dice: dice,
      named_dice: named_dice
    } do
      first_count = 1
      second_count = 2
      pool = Roll.pool([{first_count, dice}, {second_count, named_dice}])

      assert length(pool) == first_count + second_count

      assert Enum.all?(pool, fn current_dice ->
               current_dice == dice or current_dice == named_dice
             end)
    end
  end

  describe "sum/1" do
    test "properly sums results of dice rolls", %{pool: pool} do
      %{results: results, output: sum} = Roll.sum(pool)

      assert Enum.sum(results) == sum
    end

    test "always sums between the number of dice and the number of dice times the maximum result",
         %{pool: pool} do
      min = length(pool)
      max = min * 10

      assert Enum.all?(1..10, fn _ ->
               %{output: sum} = Roll.sum(pool)
               sum >= min && sum <= max
             end)
    end

    test "sums values rather than names when using a dice with named sides", %{
      named_pool: named_pool
    } do
      %{results: results, output: sum} = Roll.sum(named_pool)

      assert Enum.sum(results) == sum
    end
  end

  describe "lowest/1" do
    test "finds the lowest result", %{pool: pool} do
      %{results: results, output: lowest} = Roll.lowest(pool)
      lowest_result = Enum.min(results)

      assert lowest == lowest_result
    end

    @tag :skip
    test "finds the lowest result by name", %{named_pool: named_pool} do
      %{results: results, named_results: named_results, output: lowest} =
        Roll.lowest(named_pool)

      lowest_result = Enum.min(results)

      lowest_named_result =
        Enum.at(named_results, Enum.find_index(results, &(&1 == lowest_result)))

      assert lowest == lowest_named_result
    end
  end

  describe "highest/1" do
    test "finds the highest result", %{pool: pool} do
      %{results: results, output: highest} = Roll.highest(pool)
      highest_result = Enum.max(results)

      assert highest == highest_result
    end

    @tag :skip
    test "finds the highest result by name", %{named_pool: named_pool} do
      %{results: results, named_results: named_results, output: highest} =
        Roll.highest(named_pool)

      highest_result = Enum.max(results)

      highest_named_result =
        Enum.at(
          named_results,
          Enum.find_index(results, &(&1 == highest_result))
        )

      assert highest == highest_named_result
    end
  end

  describe "count_results/1" do
    test "displays a count of each type of result", %{pool: pool} do
      %{results: results, output: output} = Roll.count_results(pool)

      total_rolls =
        Enum.reduce(output, 0, fn {_result, count}, acc -> count + acc end)

      assert total_rolls === length(pool)
    end

    #   test "", %{named_dice: named_dice} do
    #   end
  end

  describe "count_over/2" do
    test "", %{pool: pool} do
    end

    #   test "", %{named_pool: named_pool} do
    #   end
  end

  # describe "count_under/2" do
  #   test "", %{pool: pool} do
  #   end

  #   test "", %{named_pool: named_pool} do
  #   end
  # end

  # describe "distribution/1" do
  #   test "", %{pool: pool} do
  #   end

  #   test "", %{named_pool: named_pool} do
  #   end
  # end

  # describe "average_result/2" do
  #   test "", %{pool: pool} do
  #   end

  #   test "", %{named_pool: named_pool} do
  #   end
  # end
end
