defmodule RPGDice.Repo do
  @moduledoc false

  use Ecto.Repo,
    otp_app: :rpg_dice,
    adapter: Ecto.Adapters.Postgres
end
