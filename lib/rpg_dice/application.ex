defmodule RPGDice.Application do
  @moduledoc false

  use Application

  @spec start(any, any) :: {:error, any} | {:ok, pid}
  def start(_type, _args) do
    children = [
      RPGDice.Repo
    ]

    Supervisor.start_link(children,
      strategy: :one_for_one,
      name: RPGDice.Supervisor
    )
  end
end
