defmodule RPGDice.Roll do
  @moduledoc false

  use Rop
  alias RPGDice.Dice

  @spec result(Dice.t()) :: pos_integer()
  def result(%Dice{range: range, side_names: nil}) do
    Enum.random(range)
  end

  def result(%Dice{side_names: side_names}) do
    Enum.random(side_names)
  end

  @spec pool(Keyword.t(pos_integer(), Dice.t())) :: [Dice.t()]
  def pool(counts_of_dice) do
    Enum.flat_map(counts_of_dice, fn {count, %Dice{} = dice} ->
      Enum.map(1..count, fn _ -> dice end)
    end)
  end

  @spec sum([Dice.t()]) :: %{results: [pos_integer()], output: pos_integer()}
  def sum(dice) do
    results = results(dice)

    %{results: results, output: Enum.sum(results)}
  end

  @spec lowest([Dice.t()]) :: %{
          results: [pos_integer()],
          named_results: [String.t()],
          output: pos_integer()
        }
  def lowest(dice) do
    results = results(dice)
    result = Enum.min(results)

    %{
      results: results,
      named_results: named_results(results, dice),
      output: result
    }
  end

  @spec highest([Dice.t()]) :: %{
          results: [pos_integer()],
          named_results: [String.t()],
          output: pos_integer()
        }
  def highest(dice) do
    results = results(dice)
    result = Enum.max(results)

    %{
      results: results,
      named_results: named_results(results, dice),
      output: result
    }
  end

  @spec count_results([Dice.t()]) :: %{
          results: [pos_integer()],
          output: %{required(pos_integer()) => pos_integer()}
        }
  def count_results(dice) do
    results = results(dice)

    %{results: results, output: Enum.frequencies(results)}
  end

  @spec count_over([Dice.t()], pos_integer()) :: %{
          results: [pos_integer()],
          output: pos_integer()
        }
  def count_over(dice, target_number) do
    results = results(dice)

    ouptut =
      results
      |> Enum.filter(&(&1 > target_number))
      |> length()

    %{results: results, output: ouptut}
  end

  @spec count_under([Dice.t()], pos_integer()) :: %{
          results: [pos_integer()],
          output: pos_integer()
        }
  def count_under(dice, target_number) do
    results = results(dice)

    ouptut =
      results
      |> Enum.filter(&(&1 < target_number))
      |> length()

    %{results: results, output: ouptut}
  end

  @spec distribution([Dice.t()]) :: %{
          results: [pos_integer()],
          output: {float(), float()}
        }
  def distribution(dice) do
    total = length(dice)
    %{results: results, output: counts} = count_results(dice)

    output =
      Enum.map(counts, fn {count, result} ->
        {result, total / count}
      end)

    %{results: results, output: output}
  end

  @spec average_result([Dice.t()], :mean | :median | :mode) :: %{
          results: [pos_integer()],
          output: pos_integer()
        }
  def average_result(dice, :mean) do
    %{results: results, output: sum} = sum(dice)
    output = div(sum, length(dice))

    %{results: results, output: output}
  end

  def average_result(dice, :median) do
    results = results(dice)

    middle =
      dice
      |> length()
      |> div(2)

    output =
      results
      |> Enum.sort()
      |> Enum.at(middle)

    %{results: results, output: output}
  end

  def average_result(dice, :mode) do
    %{results: results, output: count_results} = count_results(dice)

    {number, _count} =
      Enum.max_by(count_results, fn {_number, count} -> count end)

    %{results: results, output: number}
  end

  defp results(dice) do
    Enum.map(dice, fn %Dice{range: _start..end_of_range} ->
      end_of_range
      |> Dice.new()
      ~> result()
    end)
  end

  defp named_results(results, dice) do
    results
    |> Enum.zip(dice)
    |> Enum.map(fn
      {_result, %Dice{side_names: nil}} ->
        ""

      {result, %Dice{range: range, side_names: side_names}} ->
        Enum.at(side_names, Enum.find_index(range, &(&1 === result)))
    end)
  end
end
