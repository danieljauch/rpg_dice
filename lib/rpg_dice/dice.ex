defmodule RPGDice.Dice do
  @moduledoc """
  # Dice

  Dice are structs that contain attributes of a dice such that they can be
  observed or rolled.

  ## Examples

    iex> alias RPGDice.Dice
    RPGDice.Dice
    iex> Dice.new(6)
    {:ok, %RPGDice.Dice{}}

  """

  alias __MODULE__
  import RPGDiceWeb.Gettext

  @type t :: %__MODULE__{}

  @names ~w(coin d2 d4 d6 d8 d10 d12 d20 d100 d% fate fudge)

  @enforce_keys ~w(range)a
  defstruct ~w(name range side_names)a

  @spec new(pos_integer, String.t() | nil, [String.t()] | nil) ::
          {:ok, Dice.t()} | {:error, String.t()}
  def new(side_count, name \\ nil, side_names \\ nil)

  def new(sides, _name, _side_names) when sides < 2,
    do: {:error, gettext("dice must have more than 1 side")}

  def new(side_count, nil, nil) do
    {:ok, %Dice{range: 1..side_count}}
  end

  def new(_sides, "coin", _side_names) do
    {:ok, %Dice{range: 0..1, name: "coin", side_names: ~w(tails heads)}}
  end

  def new(_side_count, name, _side_names) when name in ~w(fate fudge) do
    {:ok,
     %Dice{
       range: [-1, -1, 0, 0, 1, 1],
       name: name,
       side_names: ["-", "-", " ", " ", "+", "+"]
     }}
  end

  def new(side_count, name, nil) when name != "" do
    {:ok, %Dice{range: 1..side_count, name: name}}
  end

  def new(side_count, name, side_names) when length(side_names) == side_count do
    {:ok, %Dice{range: 1..side_count, name: name, side_names: side_names}}
  end

  def new(_side_count, _name, _side_names) do
    {:error, gettext("side names don't match count of sides")}
  end

  @spec by_name(String.t()) :: {:ok, Dice.t()} | {:error, String.t()}
  def by_name(invalid_name) when invalid_name not in @names do
    {:error, gettext("no dice available with this name")}
  end

  def by_name("coin"), do: new(2, "coin")
  def by_name("d2"), do: new(2, "d2")
  def by_name("d4"), do: new(4, "d4")
  def by_name("d6"), do: new(6, "d6")
  def by_name("d8"), do: new(8, "d8")
  def by_name("d10"), do: new(10, "d10")
  def by_name("d12"), do: new(12, "d12")
  def by_name("d20"), do: new(20, "d20")
  def by_name(name) when name in ~w(d100 d%), do: new(100, name)
  def by_name(name) when name in ~w(fate fudge), do: new(6, name)
end
