defmodule RPGDiceGQL.Schema do
  @moduledoc false

  use Absinthe.Schema
  use Absinthe.Relay.Schema, :modern

  # alias RPGDiceGQL.{Mutation, Query, Subscription, Type}

  # Absinthe types
  import_types(Absinthe.Plug.Types)
  import_types(Absinthe.Type.Custom)

  # Resource types
  # import_types(Type.)

  # Query types
  # import_types(Query.)

  # Mutation types
  # import_types(Mutation.)

  # Subscription types
  # import_types(Subscription.)

  query do
    # import_fields(:_queries)
  end

  mutation do
    # import_fields(:_mutations)
  end

  subscription do
    # import_fields(:_subscriptions)
  end
end
