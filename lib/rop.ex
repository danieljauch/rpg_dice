defmodule Rop do
  @moduledoc """
  # Railway Oriented Programming

  This module provides an operator for an inline wrapper for:
  `with {:ok, response} <- call() do...end`.
  """

  defmacro __using__(_) do
    quote do
      defmacro left ~> right do
        quote do
          (fn ->
             case unquote(left) do
               {:ok, x} -> x |> unquote(right)
               expr -> expr
             end
           end).()
        end
      end
    end
  end
end
