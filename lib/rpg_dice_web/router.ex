defmodule RPGDiceWeb.Router do
  @moduledoc false

  use RPGDiceWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/api" do
    pipe_through(:api)

    forward("/graphql", Absinthe.Plug, schema: RPGDiceGQL.Schema)

    if Mix.env() == :dev do
      forward("/graphiql", Absinthe.Plug.GraphiQL, schema: RPGDiceGQL.Schema)
    end
  end
end
