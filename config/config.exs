# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of Mix.Config.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
use Mix.Config

# Configure Mix tasks and generators
config :rpg_dice,
  ecto_repos: [RPGDice.Repo]

# Configures the endpoint
config :rpg_dice, RPGDiceWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base:
    "UI6grjFAOh/79UsT3yWeB1dQE1g2KHBNNmSXuRzmnpGet8+p2BE49onv7Lala/q3",
  render_errors: [view: RPGDiceWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: RPGDiceWeb.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Gettext
config :gettext, :default_locale, "en"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
