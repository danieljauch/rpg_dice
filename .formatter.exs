# Used by "mix format"
[
  import_deps: [:ecto],
  inputs: ["*.{ex,exs}", "priv/*/seeds.exs", "{config,lib,test}/**/*.{ex,exs}"],
  line_length: 80,
  subdirectories: ["priv/*/migrations"]
]
