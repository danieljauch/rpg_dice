defmodule RPGDice.MixProject do
  use Mix.Project

  def project do
    [
      app: :rpg_dice,
      version: "0.1.0",
      elixir: "~> 1.9",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      dialyzer: [
        ignore_warnings: ".dialyzer_ignore.exs",
        plt_add_deps: :transitive,
        plt_add_apps: [:ex_unit, :mix]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {RPGDice.Application, []},
      applications: [:ex_machina, :faker],
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/factory", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:absinthe, "1.5.0-beta.2", override: true},
      {:absinthe_ecto, "~> 0.1.3"},
      {:absinthe_phoenix, "~> 1.4.0"},
      {:absinthe_plug, "~> 1.4.0"},
      {:absinthe_relay, "1.5.0-beta.0"},
      {:credo, "~> 1.1.0", only: [:dev, :test], runtime: false, override: true},
      {:credo_contrib, "~> 0.1.0-rc", only: [:dev, :test]},
      {:dialyxir, "~> 1.0.0-rc.7", only: [:dev, :test], runtime: false},
      {:ecto, "~> 3.3"},
      {:ecto_enum, "~> 1.4"},
      {:ecto_network, "~> 1.1"},
      {:ecto_sql, "~> 3.1"},
      {:ex_machina, "~> 2.3", only: [:dev, :test]},
      {:faker, "~> 0.13", only: [:dev, :test]},
      {:jason, "~> 1.0"},
      {:phoenix_ecto, "~> 4.0"},
      {:plug_cowboy, "~> 2.0"},
      {:postgrex, "~> 0.15.3"},
      {:timex, "~> 3.1"}
    ]
  end

  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"],
      lint: [
        "compile --warnings-as-errors",
        "format --check-formatted",
        "credo",
        "dialyzer"
      ]
    ]
  end
end
